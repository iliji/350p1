// D. Rivera, CSMC350 Project 1, 03/28/2021
// PrefixToPostfix.java - contains code to convert the prefix expression
// to a postfix expression

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class PrefixToPostfix {

	public String convertToPostfix(String prefixExpression) {
	
		// using regex to detect for invalid characters
		// using a negative look behind to assert that any character does not 
		// match the -,+,*,/,any digit (\d),any word (\w),and any space (\s)
		// The reason for doing it this way is that underscore (_) counts as a 
		// word character.
		Pattern pattern = Pattern.compile(".(?<![-+*/\\d\\w\\s])");
		Matcher matcher = pattern.matcher(prefixExpression);
		
		// if an invalid character is detected
		if (matcher.find()) {
			JOptionPane.showMessageDialog(null, "Invalid character detected!");
			// return and stop
			return "INVALIDCHARERROR!";
		}
		
		// using regex, add a space before and after all operators
		prefixExpression = prefixExpression.replaceAll("([+-/*])"," $1 ");
		// using regex, replace any 2 or more whitespace characters with a single space
		prefixExpression = prefixExpression.replaceAll("\\s\\s+"," ");
		// now trim any leading/trailing spaces
		prefixExpression = prefixExpression.trim();
		
		String   reverse = "";
		String[] split = prefixExpression.split(" ");
		
		// reversing the input ensuring multidigit operands like "23" stay intact
		for (int i = 0; i < split.length; i++) {
			if (i == split.length - 1) {
				reverse = split[i] + reverse;
			}
			else {
				reverse = " " + split[i] + reverse;
			}
		}// for (int i = 0; i < split.length; i++) {
		
		// setting up our stack object and tmp String placeholder
		Stack<String> operandStack = new Stack<String>();
		String tmp;
		
		// for each String in the split
		for (String s : reverse.split("\\s")) {
			
			// if it does not match an operator, push it onto the operandStack
			if (Pattern.matches("[^+-/*]+", s)) {
				operandStack.push(s);
			}
			// otherwise it is an operator
			else {
				// pop two operands from the stack, and tack on the operator at the end
				tmp = operandStack.pop() + " " + operandStack.pop() + " " + s;
				// push this String back to the operandStack
				operandStack.push(tmp);
			}// else
		}// for (String s : split) {
		
		// return the top of the stack object
		return operandStack.peek();
	}// public String convertToPostfix(String prefixExpression) {
}// public class PrefixToPostfix {
