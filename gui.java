// D. Rivera, CSMC350 Project 1, 03/28/2021
// gui.java - contains code for the main gui

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EmptyStackException;
import javax.swing.*;

public class gui extends JFrame {
	
	// calling our custom classes
	private PrefixToPostfix prefixtopostfix = new PrefixToPostfix();
	private PostfixToPrefix postfixtoprefix = new PostfixToPrefix();
	
	public gui() {
		
		super("Expression Converter");
		setSize(500,150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// top panel components
		JLabel labelEnterExpression = new JLabel("Enter Expression:");
		JTextField textEnterExpression = new JTextField(20);
		
		// top panel
		JPanel panelTop = new JPanel();
		panelTop.add(labelEnterExpression);
		panelTop.add(textEnterExpression);
		add(panelTop);
		
		// mid panel components
		JButton buttonPre2Post = new JButton("Prefix to Postfix");
		JButton buttonPost2Pre = new JButton("Postfix to Prefix");
		
		// mid panel
		JPanel panelMid = new JPanel();
		panelMid.add(buttonPre2Post);
		panelMid.add(buttonPost2Pre);
		add(panelMid);
		
		// bottom panel components
		JLabel labelResults = new JLabel("Results:");
		JTextField textResults = new JTextField(20);
		textResults.setEditable(false);
		
		// bottom panel
		JPanel panelBottom = new JPanel();
		panelBottom.add(labelResults);
		panelBottom.add(textResults);
		
		// action listener for Pre2Post button
		buttonPre2Post.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String results = prefixtopostfix.convertToPostfix(textEnterExpression.getText());
					textResults.setText(results);
				}
				catch (EmptyStackException exception) {
					JOptionPane.showMessageDialog(null, "Empty Stack!");
				}
			}// public void actionPerformed(ActionEvent e) {
		}); // buttonPre2Post.addActionListener(new ActionListener() {
		
		// action listener for Post2Pre button
		buttonPost2Pre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String results = postfixtoprefix.convertToPrefix(textEnterExpression.getText());
					textResults.setText(results);
				}
				catch (EmptyStackException exception) {
					JOptionPane.showMessageDialog(null, "Empty Stack!");
				}
			}// public void actionPerformed(ActionEvent e) {
		}); // buttonPost2Pre.addActionListener(new ActionListener() {
		
		// adding panels to frame
		getContentPane().add(BorderLayout.NORTH, panelTop);
		getContentPane().add(BorderLayout.CENTER, panelMid);
		getContentPane().add(BorderLayout.SOUTH, panelBottom);
		
		// setting visibility
		setVisible(true);
		
	}// public gui() {

	public static void main (String[] args) {
		gui app = new gui();
	}

}// public class gui extends JFrame {