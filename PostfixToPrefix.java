// D. Rivera, CSMC350 Project 1, 03/28/2021
// PostfixToPrefix.java - contains code to convert the postfix expression
// to a prefix expression

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class PostfixToPrefix {

	public String convertToPrefix(String postfixExpression) {
		
		// using regex to detect for invalid characters
		// using a negative look behind to assert that any character does not 
		// match the -,+,*,/,any digit (\d),any word (\w),and any space (\s)
		// The reason for doing it this way is that underscore (_) counts as a 
		// word character.
		Pattern pattern = Pattern.compile(".(?<![-+*/\\d\\w\\s])");
		Matcher matcher = pattern.matcher(postfixExpression);
		
		// if an invalid character is detected
		if (matcher.find()) {
			JOptionPane.showMessageDialog(null, "Invalid character detected!");
			// return and stop
			return "INVALIDCHARERROR!";
		}
		
		// using regex, add a space before and after all operators
		postfixExpression = postfixExpression.replaceAll("([+-/*])"," $1 ");
		// using regex, replace any 2 or more whitespace characters with a single space
		postfixExpression = postfixExpression.replaceAll("\\s\\s+"," ");
		// now trim any leading/trailing spaces
		postfixExpression = postfixExpression.trim();
		
		// setting up our stack object and tmp String placeholder
		Stack<String> operandStack = new Stack<String>();
		String tmp;
		
		// for each character in the String
		for (String s : postfixExpression.split("\\s")) {
			// if it does not match an operator, push it onto the operandStack
			if (Pattern.matches("[^+-/*]+", s)) {
				operandStack.push(s);
			}
			// otherwise it is an operator
			else {
				// pop two operands from the stack
				String op1 = operandStack.pop();
				String op2 = operandStack.pop();
				
				// and tack on the operator followed by the two operands in reverse order 
				tmp = s + " " + op2 + " " + op1;
				// push this String back to the operandStack
				operandStack.push(tmp);
			}// else
		}// for (String s : postfixExpression.split("\\s")) {

		// return the top of the stack object
		return operandStack.peek();
	}// public String convertToPrefix(String postfixExpression) {
}// public class PostfixToPrefix {
