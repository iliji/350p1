// D. Rivera, CSMC350 Project 1, 03/28/2021
// TestCases.java - contains code for the JUnit Tests

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class TestCases {
	
	// new instances of our conversion classes
	PrefixToPostfix pre2post = new PrefixToPostfix();
	PostfixToPrefix post2pre = new PostfixToPrefix();

	// test cases
	@Test public void TestControl() { assertEquals(2,2); }
	
	// testing pre conversion for invalid characters
	@Test public void Pre2PostTestInvalidCharacters() {
		assertEquals("INVALIDCHARERROR!", pre2post.convertToPostfix("% B C")); }
	
	// testing pre conversion, use case 1
	@Test public void Pre2PostTest1() {
		assertEquals("A B C * + D +", pre2post.convertToPostfix("+ + A * B C D")); }
	
	// testing pre conversion, use case 2
	@Test public void Pre2PostTest2() {
		assertEquals("A B + C D + *", pre2post.convertToPostfix("* + A B + C D")); }
	
	// testing pre conversion, use case 3
	@Test public void Pre2PostTest3() {
		assertEquals("A B * C D * +", pre2post.convertToPostfix("+ * A B * C D")); }
	
	// testing pre conversion, use case 4
	@Test public void Pre2PostTest4() {
		assertEquals("A B + C + D +", pre2post.convertToPostfix("+ + + A B C D")); }
	
	// testing post conversion for invalid characters
	@Test public void Post2PreTestInvalidCharacters() {
		assertEquals("INVALIDCHARERROR!", post2pre.convertToPrefix("% B C")); }
	
	// testing pre conversion, use case 1
	@Test public void Post2PreTest1() {
		assertEquals("+ + A * B C D", post2pre.convertToPrefix("A B C * + D +")); }
	
	// testing pre conversion, use case 2
	@Test public void Post2PreTest2() {
		assertEquals("* + A B + C D", post2pre.convertToPrefix("A B + C D + *")); }
	
	// testing pre conversion, use case 3
	@Test public void Post2PreTest3() {
		assertEquals("+ * A B * C D", post2pre.convertToPrefix("A B * C D * +")); }
	
	// testing pre conversion, use case 4
	@Test public void Post2PreTest4() {
		assertEquals("+ + + A B C D", post2pre.convertToPrefix("A B + C + D +")); }
	
}// public class TestCases {
